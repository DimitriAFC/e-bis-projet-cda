﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbisClasses
{
    class Borne
    {
        public int Id { get; set; }
        public int CarteIdentite { get; set; }
        public DateTimeOffset DateInstallation { get; set; }
        public DateTimeOffset DateDerniereMaintenance { get; set; }
        public int TypeChargeCodeTypeCharge { get; set; }
    }
}
