﻿
namespace E_bis
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bORNEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trouverUneBorneToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.operationChargementDesBornesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.incidentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalDesIncidentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalDesIncidentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.journalDentretienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.incidentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Controls.Add(this.menuStrip2);
            this.panel1.Controls.Add(this.menuStrip3);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 337);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(219, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(482, 34);
            this.panel2.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bORNEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 48);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(219, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bORNEToolStripMenuItem
            // 
            this.bORNEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trouverUneBorneToolStripMenuItem1,
            this.operationChargementDesBornesToolStripMenuItem});
            this.bORNEToolStripMenuItem.Name = "bORNEToolStripMenuItem";
            this.bORNEToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.bORNEToolStripMenuItem.Text = "BORNE";
            // 
            // trouverUneBorneToolStripMenuItem1
            // 
            this.trouverUneBorneToolStripMenuItem1.Name = "trouverUneBorneToolStripMenuItem1";
            this.trouverUneBorneToolStripMenuItem1.Size = new System.Drawing.Size(254, 22);
            this.trouverUneBorneToolStripMenuItem1.Text = "Recherchez une borne";
            this.trouverUneBorneToolStripMenuItem1.Click += new System.EventHandler(this.trouverUneBorneToolStripMenuItem1_Click);
            // 
            // operationChargementDesBornesToolStripMenuItem
            // 
            this.operationChargementDesBornesToolStripMenuItem.Name = "operationChargementDesBornesToolStripMenuItem";
            this.operationChargementDesBornesToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.operationChargementDesBornesToolStripMenuItem.Text = "Operation chargement des bornes";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.incidentsToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 24);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(219, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // incidentsToolStripMenuItem
            // 
            this.incidentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.journalDesIncidentsToolStripMenuItem,
            this.journalDesIncidentsToolStripMenuItem1,
            this.journalDentretienToolStripMenuItem});
            this.incidentsToolStripMenuItem.Name = "incidentsToolStripMenuItem";
            this.incidentsToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.incidentsToolStripMenuItem.Text = "Techniciens";
            this.incidentsToolStripMenuItem.Click += new System.EventHandler(this.incidentsToolStripMenuItem_Click);
            // 
            // journalDesIncidentsToolStripMenuItem
            // 
            this.journalDesIncidentsToolStripMenuItem.Name = "journalDesIncidentsToolStripMenuItem";
            this.journalDesIncidentsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.journalDesIncidentsToolStripMenuItem.Text = "Liste des techniciens";
            this.journalDesIncidentsToolStripMenuItem.Click += new System.EventHandler(this.journalDesIncidentsToolStripMenuItem_Click);
            // 
            // journalDesIncidentsToolStripMenuItem1
            // 
            this.journalDesIncidentsToolStripMenuItem1.Name = "journalDesIncidentsToolStripMenuItem1";
            this.journalDesIncidentsToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.journalDesIncidentsToolStripMenuItem1.Text = "Rechercher un technicien";
            // 
            // journalDentretienToolStripMenuItem
            // 
            this.journalDentretienToolStripMenuItem.Name = "journalDentretienToolStripMenuItem";
            this.journalDentretienToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.journalDentretienToolStripMenuItem.Text = "Journal d\'entretien";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox1.Location = new System.Drawing.Point(-3, 0);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(414, 23);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "TRON";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Location = new System.Drawing.Point(221, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(410, 22);
            this.panel3.TabIndex = 2;
            // 
            // menuStrip3
            // 
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.incidentsToolStripMenuItem1});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(219, 24);
            this.menuStrip3.TabIndex = 3;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // incidentsToolStripMenuItem1
            // 
            this.incidentsToolStripMenuItem1.Name = "incidentsToolStripMenuItem1";
            this.incidentsToolStripMenuItem1.Size = new System.Drawing.Size(75, 20);
            this.incidentsToolStripMenuItem1.Text = "Incident(s)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 340);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bORNEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trouverUneBorneToolStripMenuItem1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripMenuItem operationChargementDesBornesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem incidentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalDesIncidentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalDesIncidentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem journalDentretienToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem incidentsToolStripMenuItem1;
    }
}

